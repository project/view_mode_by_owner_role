View mody by owner role


This module allows for the modification of node display based on user
(node owner) role. To achieve this, the module requires the prior definition of
roles to be used as mapping elements, the view modes of nodes where changes
will be applied, and the new view modes that will replace the old ones. With
this configuration, the Drupal module ensures that users see nodes in the
appropriate display mode based on their assigned role, thereby enhancing the
overall user experience.


First, you have to go to the path
admin/config/system/view_mode_by_owner_role/view-mode-settings-choose-role
and select the roles that will be used as elements for mapping.

Then, in the path
admin/config/system/view_mode_by_owner_role/view-mode-settings,
you must indicate the View Modes of the Node Bundles that you want to replace.

In the third configuration option
config/system/view_mode_by_owner_role/settings-map-role-view-mode,
you specify which new View Mode will be used based on the role.
