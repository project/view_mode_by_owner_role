<?php

declare(strict_types=1);

namespace Drupal\view_mode_by_owner_role\Hooks;

use Drupal\hux\Attribute\Alter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 *
 */
final class ChangeNodeViewMode implements ContainerInjectionInterface {

  private const CONTENT_TYPE = 'node';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   *
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->view_mode_by_onwer_role = $config_factory->get('view_mode_by_owner_role.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
  *
  */
  #[Alter('entity_view_mode')]
  public function changeNodeViewMode(&$view_mode, EntityInterface $entity) {

    $values = $this->view_mode_by_onwer_role->get('map_role_view_mode');
    if ($entity->getEntityTypeId() !== 'node' || !isset($values)) {
      return;
    }

    $bundle_maped = array_keys($values);
    $view_mode_maped = array_keys($values[$entity->bundle()]);

    // Check that the bundle and the view mode are maped.
    if (in_array($entity->bundle(), $bundle_maped) &&
        in_array($view_mode, $view_mode_maped)
        ) {

      $user = $this->getOwnerByUid($entity->getOwnerId());

      $role_maped = array_intersect(
        array_keys($values[$entity->bundle()][$view_mode]),
        $user->getRoles()
      );

      $role_maped = array_keys(array_flip($role_maped));

      $view_mode = $values[$entity->bundle()][$view_mode][$role_maped[0]];
    }
  }

  /**
   *
   */
  protected function getOwnerByUid(string|int $uid):object {
    return $this->entityTypeManager->getStorage('user')->load($uid);
  }

}
