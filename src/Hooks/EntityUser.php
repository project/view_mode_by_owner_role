<?php

declare(strict_types=1);

namespace Drupal\view_mode_by_owner_role\Hooks;

use Drupal\hux\Attribute\Alter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Implement hook_form_form_id_alter for user_form and user_register_form.
 */
class EntityUser implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * Public constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->view_mode_by_onwer_role = $config_factory->get('view_mode_by_owner_role.settings');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
 * Implements hook_form_alter().
 */
  #[Alter('form_user_form')]
  public function userFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $form['#validate'][] = [$this, 'validateRoles'];
  }

  /**
 * Implements hook_form_alter().
 */
  #[Alter('user_register_form')]
  public function userRegisterFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $form['#validate'][] = [$this, 'validateRoles'];
  }

  /**
   * Implements hook_entity_type_alter().
   */
  #[Alter('entity_type')]
  public function entityTypeAlter(array &$entity_types) {
    // Add validation constraint to the user entity.
    $entity_types['user']->addConstraint('ViewModeByOwnerRoleUserRoleNumber');
  }

  /**
   * Validate that there is only one allowed role.
   */
  public function validateRoles(&$form, FormStateInterface $form_state) {
    // Get commons values in both arrays.
    $role_list = $this->view_mode_by_onwer_role->get('choosed_roles');

    $common_roles = array_intersect($form_state->getValue('roles'), $role_list);
    if (count($common_roles) > 1) {
      $list_of_roles = implode(', ', $role_list);
      $message = $this->t(
        'Please, select only one rol from this list of of roles: @list_of_roles',
        ['@list_of_roles' => $list_of_roles]
      );
      $form_state->setErrorByName('roles', $message);
    }

  }

}
