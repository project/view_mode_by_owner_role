<?php

namespace Drupal\view_mode_by_owner_role\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Validates the UserRoleNumber constraint.
 */
class UserRoleNumberConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Public consturctor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->view_mode_by_onwer_role = $config_factory->get('view_mode_by_owner_role.settings');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    $choosen_roles = $this->view_mode_by_onwer_role->get('choosed_roles');

    $common_roles = count(array_intersect($entity->getRoles(), $choosen_roles));
    if ($common_roles > 1) {
      $this->context->addViolation($constraint->roleQuantity, ['@quantity' => $common_roles]);
    }
  }

}
