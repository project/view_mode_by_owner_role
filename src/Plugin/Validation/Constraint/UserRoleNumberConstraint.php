<?php

namespace Drupal\view_mode_by_owner_role\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an UserRoleNumber constraint.
 *
 * @Constraint(
 *   id = "ViewModeByOwnerRoleUserRoleNumber",
 *   label = @Translation("UserRoleNumber", context = "Validation"),
 * )
 *
 * @DCG
 * To apply this constraint, see https://www.drupal.org/docs/drupal-apis/entity-api/entity-validation-api/providing-a-custom-validation-constraint.
 */
class UserRoleNumberConstraint extends Constraint {

  /**
   * The message that will appear if there are twoo mucho roles.
   *
   * @var string
   */
  public $roleQuantity = 'You only can choose one role from the choosed in the form "Settings Choose Role", there are @quantity selected';

  /**
   * Role quantity.
   *
   * @var string
   */
  public $quantity;

}
