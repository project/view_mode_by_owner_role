<?php

namespace Drupal\view_mode_by_owner_role\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure View mode by owner role settings for this site.
 */
class BasicSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * EntityModerationForm constructor.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity type repository.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;

  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'view_mode_by_owner_role_basic_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['view_mode_by_owner_role.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

  /**
   * Get a list of roles availables in the project.
   */
  protected function listOfRoles(): array {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    $role_list = [];
    foreach ($roles as $role) {
      $role_list[$role->id()] = $role->label();
    }

    return $role_list;
  }

  /**
   * Get the choosen roles.
   */
  protected function choosenRoles(): array {
    $config = $this->config('view_mode_by_owner_role.settings');
    $role = $config->get('choosed_roles');

    if (!$role) {
      return [];
    }

    return array_filter($role);
  }

  /**
   * Get the list of all node bunbles.
   */
  protected function listOfNodeBundles():array {
    $nodes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    $bundle_list = [];
    foreach ($nodes as $bundle) {
      $bundle_list[$bundle->id()] = $bundle->label();
    }
    return $bundle_list;
  }

  /**
   * Get the list of views mode used in entities bundles.
   */
  protected function listOfViewMode(string $bundle, string $entity = 'node'):array {
    $view_mode_list = $this->entityDisplayRepository->getViewModeOptionsByBundle($entity, $bundle);

    return $this->cleanArrayElements($view_mode_list);
  }

  /**
   * Function to merge an array with objects and array to only array.
   *
   * @param array $array
   *   Array to be clean.
   *
   * @return array
   *   Return the same array but without empty elements.
   */
  private function cleanArrayElements(array $array):array {
    $array_list = [];
    foreach ($array as $key => $value) {
      $array_list[$key] = (is_object($value)) ? $value->render() : $value;
    }
    return $array_list;
  }

}
