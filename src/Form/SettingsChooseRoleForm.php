<?php

namespace Drupal\view_mode_by_owner_role\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Configure View mode by owner role settings for this site.
 */
class SettingsChooseRoleForm extends BasicSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'view_mode_by_owner_role_settings_choosed_roles';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['view_mode_by_owner_role.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Primero.
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Select the roles that will be used to change the view mode of nodes.'),
    ];

    $form['choosed_roles'] = [
      '#required' => TRUE,
      '#type' => 'checkboxes',
      '#options' => $this->listOfRoles(),
      '#title' => $this->t('Role list'),
      '#default_value' => $this->choosenRoles(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('view_mode_by_owner_role.settings')
      ->set('choosed_roles', array_filter($form_state->getValue('choosed_roles')))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
