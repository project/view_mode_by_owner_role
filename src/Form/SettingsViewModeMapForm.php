<?php

namespace Drupal\view_mode_by_owner_role\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Configure View mode by owner role settings for this site.
 */
class SettingsViewModeMapForm extends BasicSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'view_mode_by_owner_role_view_mode_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['view_mode_by_owner_role.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // segundo.
    $bundle_list = $this->listOfNodeBundles();

    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#title' => t('Settings'),
    ];

    foreach ($bundle_list as $bundle_key => $bundle) {

      $form[$bundle_key] = [
        '#type' => 'details',
        '#title' => $bundle,
        '#group' => 'advanced',
      ];

      $view_mode_list = $this->listOfViewMode($bundle_key);
      $values = $this->config('view_mode_by_owner_role.settings')->get('view_mode_map');
      $default_value = isset($values[$bundle_key]) ? $values[$bundle_key] : [];

      $form[$bundle_key][$bundle_key] = [
        '#type' => 'checkboxes',
        '#title' => $bundle,
        '#description' => $this->t('Select those views modes that will be changed in @bundle', ['@bundle' => $bundle]),
        '#default_value' => $default_value,
        '#options' => $view_mode_list,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $bundle_list = $this->listOfNodeBundles();
    $config_to_save = $this->config('view_mode_by_owner_role.settings');

    $values_to_save = [];
    foreach ($bundle_list as $bundle_key => $bundle) {
      $selected_options = array_filter($form_state->getValue($bundle_key));

      if (!empty($selected_options)) {
        $values_to_save[$bundle_key] = $selected_options;
      }
    }

    $config_to_save->set(
       'view_mode_map',
       $values_to_save
    )->save();

    parent::submitForm($form, $form_state);
  }

}
